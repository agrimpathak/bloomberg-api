package edu.berkeley.bloombergdata.commons;

public enum OptionType {

  /**/CALL('C'),
  /**/PUT('P');

  private char name;

  OptionType(char name) {
    this.name = name;
  }

  public char getName() {
    return name;
  }
}
