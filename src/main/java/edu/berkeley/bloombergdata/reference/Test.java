package edu.berkeley.bloombergdata.reference;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.MessageIterator;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;

public class Test {

  private static final Name BAR_DATA = new Name("barData");
  private static final Name BAR_TICK_DATA = new Name("barTickData");
  private static final Name CATEGORY = new Name("category");
  private static final Name CLOSE = new Name("close");
  private static final Name HIGH = new Name("high");
  private static final Name LOW = new Name("low");
  private static final Name MESSAGE = new Name("message");
  private static final Name NUM_EVENTS = new Name("numEvents");
  private static final Name OPEN = new Name("open");
  private static final Name RESPONSE_ERROR = new Name("responseError");
  private static final Name TIME = new Name("time");
  private static final Name VOLUME = new Name("volume");

  public static void main(String[] args) throws Exception {
    System.out.println("Intraday Bars Example");
    Test example = new Test();
    example.run(args);

    System.out.println("Press ENTER to quit");
    System.in.read();
  }

  private final int d_barInterval;
  private final SimpleDateFormat d_dateFormat;
  private final DecimalFormat d_decimalFormat;
  private String d_endDateTime;
  private final String d_eventType;
  private final boolean d_gapFillInitialBar;
  private final String d_host;
  private final int d_port;
  private String d_security;
  private String d_startDateTime;

  public Test() {
    d_host = "localhost";
    d_port = 8194;
    d_barInterval = 1;
    d_security = "SPY US 11/8/2013 C170 EQUITY";
    d_security = "SPY US EQUITY";
    d_eventType = "BID";
    d_gapFillInitialBar = false;

    d_dateFormat = new SimpleDateFormat();
    d_dateFormat.applyPattern("MM/dd/yyyy k:mm");
    d_decimalFormat = new DecimalFormat();
    d_decimalFormat.setMaximumFractionDigits(3);
  }

  private void eventLoop(Session session) throws Exception {
    boolean done = false;
    while (!done) {
      Event event = session.nextEvent();
      if (event.eventType() == Event.EventType.PARTIAL_RESPONSE) {
        System.out.println("Processing Partial Response");
        processResponseEvent(event, session);
      } else if (event.eventType() == Event.EventType.RESPONSE) {
        System.out.println("Processing Response");
        processResponseEvent(event, session);
        done = true;
      } else {
        MessageIterator msgIter = event.messageIterator();
        while (msgIter.hasNext()) {
          Message msg = msgIter.next();
          System.out.println(msg);
          if (event.eventType() == Event.EventType.SESSION_STATUS) {
            if (msg.messageType().equals("SessionTerminated")) {
              done = true;
            }
          }
        }
      }
    }
  }

  private void printErrorInfo(String leadingStr, Element errorInfo) throws Exception {
    System.out.println(leadingStr + errorInfo.getElementAsString(CATEGORY) + " ("
        + errorInfo.getElementAsString(MESSAGE) + ")");
  }

  private void processMessage(Message msg) throws Exception {
    Element data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA);
    int numBars = data.numValues();
    System.out.println("Response contains " + numBars + " bars");
    System.out.println("Datetime\t\tOpen\t\tHigh\t\tLow\t\tClose" + "\t\tNumEvents\tVolume");
    for (int i = 0; i < numBars; ++i) {
      Element bar = data.getValueAsElement(i);
      Datetime time = bar.getElementAsDate(TIME);
      double open = bar.getElementAsFloat64(OPEN);
      double high = bar.getElementAsFloat64(HIGH);
      double low = bar.getElementAsFloat64(LOW);
      double close = bar.getElementAsFloat64(CLOSE);
      int numEvents = bar.getElementAsInt32(NUM_EVENTS);
      long volume = bar.getElementAsInt64(VOLUME);

      System.out.println(d_dateFormat.format(time.calendar().getTime()) + "\t"
          + d_decimalFormat.format(open) + "\t\t" + d_decimalFormat.format(high) + "\t\t"
          + d_decimalFormat.format(low) + "\t\t" + d_decimalFormat.format(close) + "\t\t"
          + d_decimalFormat.format(numEvents) + "\t\t" + d_decimalFormat.format(volume));
    }
  }

  private void processResponseEvent(Event event, Session session) throws Exception {
    MessageIterator msgIter = event.messageIterator();
    while (msgIter.hasNext()) {
      Message msg = msgIter.next();
      if (msg.hasElement(RESPONSE_ERROR)) {
        printErrorInfo("REQUEST FAILED: ", msg.getElement(RESPONSE_ERROR));
        continue;
      }
      processMessage(msg);
    }
  }

  private void run(String[] args) throws Exception {

    SessionOptions sessionOptions = new SessionOptions();
    sessionOptions.setServerHost(d_host);
    sessionOptions.setServerPort(d_port);

    System.out.println("Connecting to " + d_host + ":" + d_port);
    Session session = new Session(sessionOptions);
    if (!session.start()) {
      System.err.println("Failed to start session.");
      return;
    }
    if (!session.openService("//blp/refdata")) {
      System.err.println("Failed to open //blp/refdata");
      return;
    }

    sendIntradayBarRequest(session);

    // wait for events from session.
    // eventLoop(session);

    session.stop();
  }


  private void sendIntradayBarRequest(Session session) throws Exception {
    Service refDataService = session.getService("//blp/refdata");
    Request request = refDataService.createRequest("IntradayBarRequest");

    // only one security/eventType per request
    request.set("security", d_security);
    request.set("eventType", d_eventType);
    request.set("interval", d_barInterval);


    if ((d_startDateTime == null) || (d_endDateTime == null)) {

      request.set("startDateTime", "2014-05-23T00:00:00");
      request.set("endDateTime", "2014-05-27T23:59:00");

    } else {
      request.set("startDateTime", d_startDateTime);
      request.set("endDateTime", d_endDateTime);
    }

    if (d_gapFillInitialBar) {
      request.set("gapFillInitialBar", d_gapFillInitialBar);
    }

    System.out.println("Sending Request: " + request);
    session.sendRequest(request, null);
    eventLoop(session);
  }
}
