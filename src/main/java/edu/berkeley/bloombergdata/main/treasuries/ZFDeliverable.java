package edu.berkeley.bloombergdata.main.treasuries;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequester;

public class ZFDeliverable {

  public static void main(String[] args) throws Exception {

    String[] securities = {
        "T 1.375 02/29/2020 GOVT",
        "T 1.375 03/31/2020 GOVT",
        "T 1.375 04/30/2020 GOVT",
        "T 1.500 05/31/2020 GOVT",
        "T 1.625 06/30/2020 GOVT",
        "T 1.625 07/31/2020 GOVT",
        "T 1.375 08/31/2020 GOVT",
        "T 1.375 09/30/2020 GOVT",
        "T 1.375 10/31/2020 GOVT",
        "T 1.625 11/30/2020 GOVT",
    };
    LocalDate startDate = new LocalDate(2016, 3, 1);
    LocalDate endDate = new LocalDate(2016, 3, 31).plusDays(1);
    int timeInterval = 1;
    
    for (String security : securities) {
      String[] securityParts = security.split("\\s+");
      TreasuryIntradayBarRequestParams params = new TreasuryIntradayBarRequestParams();
      params.setSecurity(security);
      params.setTimeInterval(timeInterval);
      params.setStartDate(startDate);
      params.setEndDate(endDate);
      params.setCoupon(securityParts[1]);
      params.setMaturity(securityParts[2]);

      final String outputPath = new StringBuilder()
      /**/.append(System.getProperty("user.dir"))
      /**/.append("\\output\\")
      /**/.append(security.replaceAll("/", "-")
      /**/.replaceAll("\\s+", "_"))
      /**/.append("_")
      /**/.append(startDate.toString(YYYYMMDD))
      /**/.append("_")
      /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
      /**/.append(".csv")
      /**/.toString();

      TreasuryIntradayBarRequester requester = new TreasuryIntradayBarRequester();
      TreasuryIntradayBarRecord.initializeHeader();
      requester.initializeSession();

      for (PriceType priceType : PriceType.values()) {
        params.setEventType(priceType);
        System.out.println(params.getSecurity() + " " + priceType.toString());
        requester.sendRequest(params);
      }
      requester.stopSession();
      RecordListToCsv.writeCsv(requester.flush(), outputPath);
    }
    System.out.println("Done");
  }
}
