package edu.berkeley.bloombergdata.main.treasuries;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequester;

public class ZBDeliverable {

  public static void main(String[] args) throws Exception {

    String[] securities = {
        "T 4.500 02/15/2036 GOVT",
        "T 5.000 05/15/2037 GOVT",
        "T 4.750 02/15/2037 GOVT",
        "T 4.500 05/15/2038 GOVT",
        "T 4.375 02/15/2038 GOVT",
        "T 4.500 08/15/2039 GOVT",
        "T 4.625 02/15/2040 GOVT",
        "T 4.250 05/15/2039 GOVT",
        "T 4.375 11/15/2039 GOVT",
        "T 4.375 05/15/2040 GOVT",
        "T 4.250 11/15/2040 GOVT",
        "T 3.875 08/15/2040 GOVT",
        "T 3.500 02/15/2039 GOVT",
        "T 4.750 02/15/2041 GOVT",
    };
    LocalDate startDate = new LocalDate(2016, 3, 1);
    LocalDate endDate = new LocalDate(2016, 3, 31).plusDays(1);
    int timeInterval = 1;
    
    for (String security : securities) {
      String[] securityParts = security.split("\\s+");
      TreasuryIntradayBarRequestParams params = new TreasuryIntradayBarRequestParams();
      params.setSecurity(security);
      params.setTimeInterval(timeInterval);
      params.setStartDate(startDate);
      params.setEndDate(endDate);
      params.setCoupon(securityParts[1]);
      params.setMaturity(securityParts[2]);

      final String outputPath = new StringBuilder()
      /**/.append(System.getProperty("user.dir"))
      /**/.append("\\output\\")
      /**/.append(security.replaceAll("/", "-")
      /**/.replaceAll("\\s+", "_"))
      /**/.append("_")
      /**/.append(startDate.toString(YYYYMMDD))
      /**/.append("_")
      /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
      /**/.append(".csv")
      /**/.toString();

      TreasuryIntradayBarRequester requester = new TreasuryIntradayBarRequester();
      TreasuryIntradayBarRecord.initializeHeader();
      requester.initializeSession();

      for (PriceType priceType : PriceType.values()) {
        params.setEventType(priceType);
        System.out.println(params.getSecurity() + " " + priceType.toString());
        requester.sendRequest(params);
      }
      requester.stopSession();
      RecordListToCsv.writeCsv(requester.flush(), outputPath);
    }
    System.out.println("Done");
  }
}
