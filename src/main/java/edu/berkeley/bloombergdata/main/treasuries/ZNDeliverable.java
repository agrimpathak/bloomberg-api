package edu.berkeley.bloombergdata.main.treasuries;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.bond.TreasuryIntradayBarRequester;

public class ZNDeliverable {

  public static void main(String[] args) throws Exception {

    String[] securities = {
        "T 1.875 08/31/2022 GOVT",
        "T 2.125 06/30/2022 GOVT",
        "T 2.000 07/31/2022 GOVT",
        "T 1.625 08/15/2022 GOVT",
        "T 1.625 11/15/2022 GOVT",
        "T 1.750 09/30/2022 GOVT",
        "T 2.000 02/15/2023 GOVT",
        "T 1.750 05/15/2023 GOVT",
        "T 2.500 08/15/2023 GOVT",
        "T 2.750 11/15/2023 GOVT",
        "T 2.750 02/15/2024 GOVT",
        "T 2.500 05/15/2024 GOVT",
        "T 2.375 08/15/2024 GOVT",
        "T 2.250 11/15/2024 GOVT",
        "T 2.000 02/15/2025 GOVT",
        "T 2.125 05/15/2025 GOVT",
        "T 2.000 08/15/2025 GOVT",
        "T 1.875 10/31/2022 GOVT",
        "T 2.000 11/30/2022 GOVT",
        "T 2.250 11/15/2025 GOVT",
    };
    LocalDate startDate = new LocalDate(2016, 3, 1);
    LocalDate endDate = new LocalDate(2016, 3, 31).plusDays(1);
    int timeInterval = 1;
    
    for (String security : securities) {
      String[] securityParts = security.split("\\s+");
      TreasuryIntradayBarRequestParams params = new TreasuryIntradayBarRequestParams();
      params.setSecurity(security);
      params.setTimeInterval(timeInterval);
      params.setStartDate(startDate);
      params.setEndDate(endDate);
      params.setCoupon(securityParts[1]);
      params.setMaturity(securityParts[2]);

      final String outputPath = new StringBuilder()
      /**/.append(System.getProperty("user.dir"))
      /**/.append("\\output\\")
      /**/.append(security.replaceAll("/", "-")
      /**/.replaceAll("\\s+", "_"))
      /**/.append("_")
      /**/.append(startDate.toString(YYYYMMDD))
      /**/.append("_")
      /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
      /**/.append(".csv")
      /**/.toString();

      TreasuryIntradayBarRequester requester = new TreasuryIntradayBarRequester();
      TreasuryIntradayBarRecord.initializeHeader();
      requester.initializeSession();

      for (PriceType priceType : PriceType.values()) {
        params.setEventType(priceType);
        System.out.println(params.getSecurity() + " " + priceType.toString());
        requester.sendRequest(params);
      }
      requester.stopSession();
      RecordListToCsv.writeCsv(requester.flush(), outputPath);
    }
    System.out.println("Done");
  }
}
