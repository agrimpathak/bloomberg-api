package edu.berkeley.bloombergdata.main.stock;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequester;

public class IWM {

  public static void main(String[] args) throws Exception {

    String security = "IWM US EQUITY";
    LocalDate startDate = new LocalDate(2017, 1, 1);
    LocalDate endDate = new LocalDate(2017, 1, 31).plusDays(1);
    int timeInterval = 1;

    IntradayBarRequestParams params = new IntradayBarRequestParams();
    params.setSecurity(security);
    params.setTimeInterval(timeInterval);
    params.setStartDate(startDate);
    params.setEndDate(endDate);

    final String outputPath = new StringBuilder()
    /**/.append("C:\\Users\\apathak\\Programming\\BloombergData\\output\\")
    /**/.append(security.replaceAll("\\s+", ""))
    /**/.append("_")
    /**/.append(startDate.toString(YYYYMMDD))
    /**/.append("_")
    /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
    /**/.append(".csv")
    /**/.toString();

    IntradayBarRequester requester = new IntradayBarRequester();
    requester.initializeSession();
    IntradayBarRecord.initializeHeader();

    for (PriceType priceType : PriceType.values()) {
      params.setEventType(priceType);
      System.out.println(params.getSecurity() + " " + priceType.toString());
      requester.sendRequest(params);
    }
    requester.stopSession();
    RecordListToCsv.writeCsv(requester.flush(), outputPath);
    System.out.println("Done");
  }
}
