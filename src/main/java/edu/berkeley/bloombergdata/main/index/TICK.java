package edu.berkeley.bloombergdata.main.index;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.index.IndexIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.index.IndexIntradayBarRequester;

public class TICK {

  public static void main(String[] args) throws Exception {

    String security = "TICK INDEX";
    LocalDate startDate = new LocalDate(2017, 1, 1);
    LocalDate endDate = new LocalDate(2017, 1, 31).plusDays(1);
    int timeInterval = 1;

    IntradayBarRequestParams params = new IntradayBarRequestParams();
    params.setSecurity(security);
    params.setTimeInterval(timeInterval);
    params.setStartDate(startDate);
    params.setEndDate(endDate);
    params.setEventType(PriceType.TRADE);

    final String outputPath = new StringBuilder()
    /**/.append("C:\\Users\\apathak\\Programming\\BloombergData\\output\\")
    /**/.append(security.replaceAll("\\s+", ""))
    /**/.append("_")
    /**/.append(startDate.toString(YYYYMMDD))
    /**/.append("_")
    /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
    /**/.append(".csv")
    /**/.toString();

    IndexIntradayBarRequester requester = new IndexIntradayBarRequester();
    requester.initializeSession();
    IndexIntradayBarRecord.initializeHeader();

    System.out.println(params.getSecurity());
    requester.sendRequest(params);
    requester.stopSession();
    RecordListToCsv.writeCsv(requester.flush(), outputPath);
    System.out.println("Done");
  }
}
