package edu.berkeley.bloombergdata.main.futures;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRequester;

public class ZN {

  public static void main(String[] args) throws Exception {

    String security = "TYU6 COMDTY";
    LocalDate startDate = new LocalDate(2016, 1, 1);
    LocalDate endDate = new LocalDate(2016, 1, 31).plusDays(1);
    int timeInterval = 1;

    FuturesIntradayBarRequestParams params = new FuturesIntradayBarRequestParams();
    params.setSecurity(security);
    params.setTimeInterval(timeInterval);
    params.setStartDate(startDate);
    params.setEndDate(endDate);
    params.setExchangeSymbol("ZN");
    params.setUnderlying("US 10yr 6%");

    final String outputPath = new StringBuilder()
    /**/.append(System.getProperty("user.dir"))
    /**/.append("\\output\\")
    /**/.append(security.replaceAll("\\s+", ""))
    /**/.append("_")
    /**/.append(startDate.toString(YYYYMMDD))
    /**/.append("_")
    /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
    /**/.append(".csv")
    /**/.toString();

    FuturesIntradayBarRequester requester = new FuturesIntradayBarRequester();
    FuturesIntradayBarRecord.initializeHeader();
    requester.initializeSession();

    for (PriceType priceType : PriceType.values()) {
      params.setEventType(priceType);
      System.out.println(params.getSecurity() + " " + priceType.toString());
      requester.sendRequest(params);
    }
    requester.stopSession();
    RecordListToCsv.writeCsv(requester.flush(), outputPath);
    System.out.println("Done");
  }
}
