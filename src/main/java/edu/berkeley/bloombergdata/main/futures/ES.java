package edu.berkeley.bloombergdata.main.futures;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.futures.FuturesIntradayBarRequester;

public class ES {

  public static void main(String[] args) throws Exception {

    String security = "ESM7 INDEX";
    LocalDate startDate = new LocalDate(2017, 1, 1);
    LocalDate endDate = new LocalDate(2017, 1, 31).plusDays(1);
    int timeInterval = 1;

    FuturesIntradayBarRequestParams params = new FuturesIntradayBarRequestParams();
    params.setSecurity(security);
    params.setTimeInterval(timeInterval);
    params.setStartDate(startDate);
    params.setEndDate(endDate);
    params.setExchangeSymbol("ES");
    params.setUnderlying("SPX INDEX");

    final String outputPath = new StringBuilder()
    /**/.append("C:\\Users\\apathak\\Programming\\BloombergData\\output\\")
    /**/.append(security.replaceAll("\\s+", ""))
    /**/.append("_")
    /**/.append(startDate.toString(YYYYMMDD))
    /**/.append("_")
    /**/.append(endDate.minusDays(1).toString(YYYYMMDD))
    /**/.append(".csv")
    /**/.toString();

    FuturesIntradayBarRequester requester = new FuturesIntradayBarRequester();
    FuturesIntradayBarRecord.initializeHeader();
    requester.initializeSession();

    for (PriceType priceType : PriceType.values()) {
      params.setEventType(priceType);
      System.out.println(params.getSecurity() + " " + priceType.toString());
      requester.sendRequest(params);
    }
    requester.stopSession();
    RecordListToCsv.writeCsv(requester.flush(), outputPath);
    System.out.println("Done");
  }
}
