package edu.berkeley.bloombergdata.main.options;

import static edu.berkeley.bloombergdata.util.Formatting.YYYYMMDD;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.OptionType;
import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.RecordListToCsv;
import edu.berkeley.bloombergdata.intradaybar.option.OptionIntradayBarRecord;
import edu.berkeley.bloombergdata.intradaybar.option.OptionIntradayBarRequestParams;
import edu.berkeley.bloombergdata.intradaybar.option.OptionIntradayBarRequester;

public class SPXOption {

  public static void main(String[] args) throws Exception {

    String underlying = "SPX INDEX";
    LocalDate startDate = new LocalDate().minusWeeks(7);
    LocalDate endDate = new LocalDate().plusDays(1);
    LocalDate expirationDate = new LocalDate(2017, 1, 20);

    int timeInterval = 1;
    double minStrike = 1500;
    double maxStrike = 2500;
    double strikeInterval = 5.0;

    OptionIntradayBarRequestParams params = new OptionIntradayBarRequestParams();
    params.setUnderlying(underlying);
    params.setTimeInterval(timeInterval);
    params.setStartDate(startDate);
    params.setEndDate(endDate);
    params.setExpirationDate(expirationDate);
    params.setSuffix("EQUITY");

    final String outputPath = new StringBuilder()
    /**/.append("C:\\Users\\apathak\\Programming\\BloombergData\\output\\")
    /**/.append(underlying.split("\\s+")[0])
    /**/.append("_Option_")
    /**/.append(expirationDate.toString(YYYYMMDD))
    /**/.append(".csv")
    /**/.toString();

    OptionIntradayBarRequester requester = new OptionIntradayBarRequester();
    OptionIntradayBarRecord.initializeHeader();
    requester.initializeSession();

    for (double strike = minStrike; strike <= maxStrike; strike += strikeInterval) {
      for (OptionType optionType : OptionType.values()) {
        for (PriceType priceType : PriceType.values()) {
          params.setStrike(strike);
          params.setOptionType(optionType);
          params.setEventType(priceType);
          System.out.println(params.getSecurity() + " " + priceType.toString());
          requester.splitAndSendRequest(params);
        }
      }
    }
    requester.stopSession();
    RecordListToCsv.writeCsv(requester.flush(), outputPath);
    System.out.println("Done");
  }
}
