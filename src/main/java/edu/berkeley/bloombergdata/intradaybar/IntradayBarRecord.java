package edu.berkeley.bloombergdata.intradaybar;

import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.Map;

import com.bloomberglp.blpapi.Datetime;

import edu.berkeley.bloombergdata.commons.PriceType;
import edu.berkeley.bloombergdata.export.Record;

public class IntradayBarRecord implements Record {

  protected static String[] header;
  protected final static String HEADER_CLOSE = "close";
  protected final static String HEADER_EVENTCOUNT = "event_count";
  protected final static String HEADER_HIGH = "high";
  protected final static String HEADER_LOW = "low";
  protected final static String HEADER_OPEN = "open";
  protected final static String HEADER_PRICE_TYPE = "price_type";
  protected final static String HEADER_SECURITY = "security";
  protected final static String HEADER_TIMESTAMP = "price_timestamp";
  protected final static String HEADER_VOLUME = "volume";

  public static void initializeHeader() {
    final int FIELD_COUNT = 9;
    header = new String[FIELD_COUNT];
    int i = 0;
    header[i++] = HEADER_TIMESTAMP;
    header[i++] = HEADER_SECURITY;
    header[i++] = HEADER_PRICE_TYPE;
    header[i++] = HEADER_OPEN;
    header[i++] = HEADER_HIGH;
    header[i++] = HEADER_LOW;
    header[i++] = HEADER_CLOSE;
    header[i++] = HEADER_VOLUME;
    header[i++] = HEADER_EVENTCOUNT;
  }

  private final Map<String, Object> contents = new HashMap<String, Object>();

  @Override
  public Map<String, Object> getEntries() {
    return contents;
  }

  @Override
  public String[] header() {
    return header;
  }

  public void setClose(double close) {
    setGeneric(HEADER_CLOSE, "" + close);
  }

  public void setDatetime(Datetime datetime) {
    setGeneric(HEADER_TIMESTAMP, datetime.toString());
  }

  public void setEventCount(int eventCount) {
    setGeneric(HEADER_EVENTCOUNT, "" + eventCount);
  }

  protected void setGeneric(String field, String entry) {
    contents.put(field, entry);
  }

  public void setHigh(double high) {
    setGeneric(HEADER_HIGH, "" + high);
  }

  public void setLow(double low) {
    setGeneric(HEADER_LOW, "" + low);
  }

  public void setOpen(double open) {
    setGeneric(HEADER_OPEN, "" + open);
  }

  public void setPriceType(PriceType priceType) {
    setGeneric(HEADER_PRICE_TYPE, priceType.toString());
  }

  public void setSecurity(String security) {
    setGeneric(HEADER_SECURITY, security);
  }

  public void setVolume(long volume) {
    setGeneric(HEADER_VOLUME, "" + volume);
  }

  @Override
  public void validate() {
    for (String _header : header) {
      checkNotNull(contents.get(_header));
    }
  }
}
