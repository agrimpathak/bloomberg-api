package edu.berkeley.bloombergdata.intradaybar.futures;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRecord;

public class FuturesIntradayBarRecord extends IntradayBarRecord {

  private final static String HEADER_EXCHANGE_SYMBOL = "exchange_symbol";
  private final static String HEADER_UNDERLYING = "underlying";

  public static void initializeHeader() {
    final int FIELD_COUNT = 11;
    header = new String[FIELD_COUNT];
    int i = 0;
    header[i++] = HEADER_TIMESTAMP;
    header[i++] = HEADER_SECURITY;
    header[i++] = HEADER_PRICE_TYPE;
    header[i++] = HEADER_OPEN;
    header[i++] = HEADER_HIGH;
    header[i++] = HEADER_LOW;
    header[i++] = HEADER_CLOSE;
    header[i++] = HEADER_VOLUME;
    header[i++] = HEADER_EVENTCOUNT;
    header[i++] = HEADER_UNDERLYING;
    header[i++] = HEADER_EXCHANGE_SYMBOL;
  }

  void setExchangeSymbol(String baseTicker) {
    setGeneric(HEADER_EXCHANGE_SYMBOL, baseTicker);
  }

  void setUnderlying(String underlying) {
    setGeneric(HEADER_UNDERLYING, underlying);
  }
}
