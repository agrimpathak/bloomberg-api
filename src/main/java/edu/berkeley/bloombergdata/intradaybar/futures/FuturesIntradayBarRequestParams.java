package edu.berkeley.bloombergdata.intradaybar.futures;

import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequestParams;

public class FuturesIntradayBarRequestParams extends IntradayBarRequestParams {

  private String exchangeSymbol;
  private String underlying;

  public String getExchangeSymbol() {
    return exchangeSymbol;
  }

  public String getUnderlying() {
    return underlying;
  }

  public void setExchangeSymbol(String exchangeSymbol) {
    this.exchangeSymbol = exchangeSymbol;
  }

  public void setUnderlying(String underlying) {
    this.underlying = underlying;
  }

  @Override
  public void validateParams() {
    super.validateParams();
    checkNotNull(exchangeSymbol, underlying);
  }
}
