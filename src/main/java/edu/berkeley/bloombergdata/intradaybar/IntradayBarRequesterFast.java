package edu.berkeley.bloombergdata.intradaybar;

import static edu.berkeley.bloombergdata.util.Formatting.DATE_TIME_FORMAT_0;
import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;
import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.MessageIterator;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;

import edu.berkeley.bloombergdata.export.Record;

public class IntradayBarRequesterFast {

  protected static final Name BAR_DATA = new Name("barData");
  protected static final Name BAR_TICK_DATA = new Name("barTickData");
  private static final Name CATEGORY = new Name("category");
  protected static final Name CLOSE = new Name("close");
  protected static final Name HIGH = new Name("high");
  protected static final Name LOW = new Name("low");
  private static final int MAX_ERROR_COUNT = 15;
  private static final Name MESSAGE = new Name("message");
  protected static final Name NUM_EVENTS = new Name("numEvents");
  protected static final Name OPEN = new Name("open");
  private static final Name RESPONSE_ERROR = new Name("responseError");
  protected static final Name TIME = new Name("time");
  protected static final Name VOLUME = new Name("volume");

  protected final Map<CorrelationID, IntradayBarRequestParams> corridToParams =
  /**/new HashMap<CorrelationID, IntradayBarRequestParams>();
  private final SimpleDateFormat d_dateFormat = new SimpleDateFormat();
  private final DecimalFormat d_decimalFormat = new DecimalFormat();
  private final String d_host = "localhost";
  private final int d_port = 8194;
  private int errorCtr = 0;
  protected List<Record> newRecords;
  private List<Record> records = new LinkedList<Record>();
  private long requestSeed = 1L;
  private Session session;

  public IntradayBarRequesterFast() {
    d_dateFormat.applyPattern("MM/dd/yyyy k:mm");
    d_decimalFormat.setMaximumFractionDigits(3);
  }

  public List<Record> flush() {
    List<Record> temp = records;
    records = new LinkedList<Record>();
    return temp;
  }

  public void initializeSession() throws Exception {
    SessionOptions sessionOptions = new SessionOptions();
    sessionOptions.setServerHost(d_host);
    sessionOptions.setServerPort(d_port);
    if (session != null) {
      stopSession();
    }
    session = new Session(sessionOptions);
    checkArgument(session.start(), "Failed to start session.");
    checkArgument(session.openService("//blp/refdata"), "Failed to open //blp/refdata");
  }

  private void printErrorInfo(String leadingStr, Element errorInfo) throws Exception {
    System.out.println(leadingStr + errorInfo.getElementAsString(CATEGORY) + " ("
        + errorInfo.getElementAsString(MESSAGE) + ")");
  }

  /**
   * Processes the event.
   *
   * @param event
   * @return A mapping of CorrelationID to Boolean. The boolean indicates whether an error is
   *         associated with the given CorrelationID
   * @throws Exception
   */
  private Map<CorrelationID, Boolean> processEventResponses(Event event) throws Exception {
    MessageIterator msgIter = event.messageIterator();
    Map<CorrelationID, Boolean> corrIdToError = new HashMap<CorrelationID, Boolean>();
    boolean timedOut = false;
    System.out.println("iterating through messages");
    while (msgIter.hasNext()) {
      System.out.println("processing a message");
      Message msg = msgIter.next();
      CorrelationID corrId = msg.correlationID();
      /*
       * If timed out, mark all correlationIds associated with the incoming messages as an error in
       * order to re-request
       */
      if (timedOut) {
        corrIdToError.put(corrId, true);
      } else if (msg.hasElement(RESPONSE_ERROR)) {
        /*
         * If the security does not exist, remove from corridToParams and resume
         */
        if (msg.getElement(RESPONSE_ERROR).getElementAsString(CATEGORY).equals("BAD_SEC")) {
          errorCtr = 0;
          String security = corridToParams.remove(corrId).getSecurity();
          System.out.println("Resuming on BAD_SEC: " + security);
          continue;
        }
        corrIdToError.put(corrId, true);
        errorCtr++;
        printErrorInfo("REQUEST FAILED: ", msg.getElement(RESPONSE_ERROR));
        /*
         * If the response error is a time out, then reconnect
         */
        if (msg.getElement(RESPONSE_ERROR).getElementAsString(CATEGORY).equals("TIMEOUT")) {
          initializeSession();
          timedOut = true;
        }
        /*
         * Else, process the message only if there is no error associated with the correlationId
         */
      } else if (!corrIdToError.containsKey(corrId) || !corrIdToError.get(corrId)) {
        processMessage(msg);
      }
    }
    return corrIdToError;
  }

  protected void processMessage(Message msg) throws Exception {
    Element data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA);
    int numBars = data.numValues();
    IntradayBarRecord nextRecord;
    IntradayBarRequestParams recievedRequestParams = corridToParams.get(msg.correlationID());
    for (int i = 0; i < numBars; ++i) {
      Element bar = data.getValueAsElement(i);
      Datetime time = bar.getElementAsDate(TIME);
      double open = bar.getElementAsFloat64(OPEN);
      double high = bar.getElementAsFloat64(HIGH);
      double low = bar.getElementAsFloat64(LOW);
      double close = bar.getElementAsFloat64(CLOSE);
      int numEvents = bar.getElementAsInt32(NUM_EVENTS);
      long volume = bar.getElementAsInt64(VOLUME);

      nextRecord = new IntradayBarRecord();
      nextRecord.setDatetime(time);
      nextRecord.setSecurity(recievedRequestParams.getSecurity());
      nextRecord.setPriceType(recievedRequestParams.getPriceType());
      nextRecord.setOpen(open);
      nextRecord.setHigh(high);
      nextRecord.setLow(low);
      nextRecord.setClose(close);
      nextRecord.setEventCount(numEvents);
      nextRecord.setVolume(volume);
      nextRecord.validate();
      newRecords.add(nextRecord);
    }
  }

  public void processSessionEvents() throws Exception {
    while (!corridToParams.isEmpty()) {
      System.out.println("corrIDToParams size: " + corridToParams.size());
      for (IntradayBarRequestParams p : corridToParams.values()) {
        System.out.println("\t" + p.getSecurity());
      }
      Event event = session.nextEvent();
      System.out.println("done waiting for next event");
      if ((event.eventType() == Event.EventType.PARTIAL_RESPONSE) ||
      /**/(event.eventType() == Event.EventType.RESPONSE)) {
        /*
         * If errorCorrId is not null, then there is a request error, so resend
         */
        System.out.println("About to process event...");
        Map<CorrelationID, Boolean> corridToError = processEventResponses(event); 
        for (Map.Entry<CorrelationID, Boolean> entry : corridToError.entrySet()) {
          /*
           * If there's an error associated with the correlationID, then resend
           */
          System.out.println("processing event");
          if (entry.getValue()) {
            IntradayBarRequestParams resendParams = corridToParams.remove(entry.getKey());
            sendRequest(resendParams);
            System.out.println("Resending...");
            /*
             * If the event is a full response, remove the correlationID from corridToParams because
             * there are no more incoming data available for the associated params
             */
          } else if (event.eventType() == Event.EventType.RESPONSE) {
            System.out.println("Recieved response.  corrIdToParams size: " + corridToParams.size());
            corridToParams.remove(entry.getKey());
          }
        }
      } else {
        System.out.println("Event wasn't a PR or R");
        MessageIterator msgIter = event.messageIterator();
        while (msgIter.hasNext()) {
          Message msg = msgIter.next();
          if (event.eventType() == Event.EventType.SESSION_STATUS) {
            if (msg.messageType().equals("SessionTerminated")) {
              errorCtr = 0;
              records.addAll(newRecords);
              newRecords = null;
              return;
            }
          }
        }
      }
    }
  }

  public void sendRequest(IntradayBarRequestParams params) throws Exception {
    checkNotNull(session);
    // checkArgument(session.start());
    checkArgument(errorCtr <= MAX_ERROR_COUNT);
    params.validateParams();
    newRecords = new LinkedList<Record>();
    /*
     * Create and send the request...
     */
    Service refDataService = session.getService("//blp/refdata");
    Request request = refDataService.createRequest("IntradayBarRequest");
    request.set("startDateTime",
    /**/params.getStartDate().toString(DATE_TIME_FORMAT_0).replace(" ", "T"));
    request.set("endDateTime", params.getEndDate().toString(DATE_TIME_FORMAT_0).replace(" ", "T"));
    request.set("security", params.getSecurity());
    request.set("eventType", params.getPriceType().toString());
    request.set("interval", params.getInterval());
    request.set("gapFillInitialBar", params.getGapFillInitialBar());
    CorrelationID corrId = new CorrelationID(requestSeed++);
    session.sendRequest(request, corrId);
    corridToParams.put(corrId, params);
    /*
     * Wait for events from session
     */
    // processSessionEvents();
  }

  /**
   * Break up the request into smaller time blocks and send. This is done to prevent the session
   * from timing out on large request.
   *
   * @param params
   * @throws Exception
   */
  public void splitAndSendRequest(IntradayBarRequestParams params) throws Exception {
    LocalDate endDate;
    final LocalDate trueStartDate = params.getStartDate();
    final LocalDate trueEndDate = params.getEndDate();
    do {
      endDate = params.getStartDate().plusWeeks(2);
      if (endDate.isAfter(trueEndDate)) {
        endDate = trueEndDate;
      }
      // System.out.println(params.getStartDate() + " - " + endDate);
      params.setEndDate(endDate);
      sendRequest(params);
      params.setStartDate(endDate);
    } while (params.getStartDate().isBefore(trueEndDate));
    params.setStartDate(trueStartDate);
    params.setEndDate(trueEndDate);
  }

  public void stopSession() throws InterruptedException {
    session.stop();
  }
}
