package edu.berkeley.bloombergdata.intradaybar.index;

import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Message;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequester;

public class IndexIntradayBarRequester extends IntradayBarRequester {

  @Override
  protected void processMessage(Message msg) throws Exception {
    Element data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA);
    int numBars = data.numValues();
    IndexIntradayBarRecord nextRecord;
    for (int i = 0; i < numBars; ++i) {
      Element bar = data.getValueAsElement(i);
      Datetime time = bar.getElementAsDate(TIME);
      double open = bar.getElementAsFloat64(OPEN);
      double high = bar.getElementAsFloat64(HIGH);
      double low = bar.getElementAsFloat64(LOW);
      double close = bar.getElementAsFloat64(CLOSE);

      nextRecord = new IndexIntradayBarRecord();
      nextRecord.setDatetime(time);
      nextRecord.setSecurity(lastRequestParams.getSecurity());
      nextRecord.setOpen(open);
      nextRecord.setHigh(high);
      nextRecord.setLow(low);
      nextRecord.setClose(close);
      nextRecord.validate();
      newRecords.add(nextRecord);
    }
  }
}
