package edu.berkeley.bloombergdata.intradaybar.index;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRecord;

public class IndexIntradayBarRecord extends IntradayBarRecord {

  public static void initializeHeader() {
    final int FIELD_COUNT = 6;
    header = new String[FIELD_COUNT];
    int i = 0;
    header[i++] = HEADER_TIMESTAMP;
    header[i++] = HEADER_SECURITY;
    header[i++] = HEADER_OPEN;
    header[i++] = HEADER_HIGH;
    header[i++] = HEADER_LOW;
    header[i++] = HEADER_CLOSE;
  }
}
