package edu.berkeley.bloombergdata.intradaybar;

import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;
import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.PriceType;

public class IntradayBarRequestParams {

  private int barInterval;
  protected LocalDate endDate;
  private boolean gapFillInitialBar = false;
  protected PriceType priceType;
  private String security;
  protected LocalDate startDate;

  public LocalDate getEndDate() {
    return endDate;
  }

  public boolean getGapFillInitialBar() {
    return gapFillInitialBar;
  }

  public int getInterval() {
    return barInterval;
  }

  public PriceType getPriceType() {
    return priceType;
  }

  public String getSecurity() {
    return security;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public void setEventType(PriceType eventType) {
    priceType = eventType;
  }

  public void setGapFillInitialBar(boolean gapFillInitialBar) {
    this.gapFillInitialBar = gapFillInitialBar;
  }

  public void setSecurity(String security) {
    this.security = security;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public void setTimeInterval(int barInterval) {
    this.barInterval = barInterval;
  }

  public void validateParams() {
    checkArgument(!security.isEmpty());
    checkNotNull(startDate, endDate, priceType);
  }
}
