package edu.berkeley.bloombergdata.intradaybar;

import static edu.berkeley.bloombergdata.util.Formatting.DATE_TIME_FORMAT_0;
import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;
import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.LocalDate;

import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.MessageIterator;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;

import edu.berkeley.bloombergdata.export.Record;

/**
 *
 * @author Agrim Pathak
 */
public class IntradayBarRequester {

  protected static final Name BAR_DATA = new Name("barData");
  protected static final Name BAR_TICK_DATA = new Name("barTickData");
  private static final Name CATEGORY = new Name("category");
  protected static final Name CLOSE = new Name("close");
  protected static final Name HIGH = new Name("high");
  protected static final Name LOW = new Name("low");
  private static final int MAX_ERROR_COUNT = 15;
  private static final Name MESSAGE = new Name("message");
  protected static final Name NUM_EVENTS = new Name("numEvents");
  protected static final Name OPEN = new Name("open");
  private static final Name RESPONSE_ERROR = new Name("responseError");
  protected static final Name TIME = new Name("time");
  protected static final Name VOLUME = new Name("volume");

  private final SimpleDateFormat d_dateFormat = new SimpleDateFormat();
  private final DecimalFormat d_decimalFormat = new DecimalFormat();
  private final String d_host = "localhost";
  private final int d_port = 8194;
  private int errorCtr = 0;
  protected IntradayBarRequestParams lastRequestParams;
  protected List<Record> newRecords;
  private List<Record> records = new LinkedList<Record>();
  private Session session;

  public IntradayBarRequester() {
    d_dateFormat.applyPattern("MM/dd/yyyy k:mm");
    d_decimalFormat.setMaximumFractionDigits(3);
  }

  private void eventLoop(Session session) throws Exception {
    boolean done = false;
    while (!done) {
      Event event = session.nextEvent();
      if ((event.eventType() == Event.EventType.PARTIAL_RESPONSE) ||
      /**/(event.eventType() == Event.EventType.RESPONSE)) {
        /*
         * If request error, resend
         */
        if (!processResponseEvent(event, session)) {
          sendRequest(lastRequestParams);
          return;
        }
        if (event.eventType() == Event.EventType.RESPONSE) {
          done = true;
        }
      } else {
        MessageIterator msgIter = event.messageIterator();
        while (msgIter.hasNext()) {
          Message msg = msgIter.next();
          if (event.eventType() == Event.EventType.SESSION_STATUS) {
            if (msg.messageType().equals("SessionTerminated")) {
              done = true;
            }
          }
        }
      }
    }
    errorCtr = 0;
    records.addAll(newRecords);
    newRecords = null;
  }

  public List<Record> flush() {
    List<Record> temp = records;
    records = new LinkedList<Record>();
    return temp;
  }

  public void initializeSession() throws Exception {
    SessionOptions sessionOptions = new SessionOptions();
    sessionOptions.setServerHost(d_host);
    sessionOptions.setServerPort(d_port);
    if (session != null) {
      stopSession();
    }
    session = new Session(sessionOptions);
    checkArgument(session.start(), "Failed to start session.");
    checkArgument(session.openService("//blp/refdata"), "Failed to open //blp/refdata");
  }

  private void printErrorInfo(String leadingStr, Element errorInfo) throws Exception {
    System.out.println(leadingStr + errorInfo.getElementAsString(CATEGORY) + " ("
        + errorInfo.getElementAsString(MESSAGE) + ")");
  }

  protected void processMessage(Message msg) throws Exception {
    Element data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA);
    int numBars = data.numValues();
    IntradayBarRecord nextRecord;
    for (int i = 0; i < numBars; ++i) {
      Element bar = data.getValueAsElement(i);
      Datetime time = bar.getElementAsDate(TIME);
      double open = bar.getElementAsFloat64(OPEN);
      double high = bar.getElementAsFloat64(HIGH);
      double low = bar.getElementAsFloat64(LOW);
      double close = bar.getElementAsFloat64(CLOSE);
      int numEvents = bar.getElementAsInt32(NUM_EVENTS);
      long volume = bar.getElementAsInt64(VOLUME);

      nextRecord = new IntradayBarRecord();
      nextRecord.setDatetime(time);
      nextRecord.setSecurity(lastRequestParams.getSecurity());
      nextRecord.setPriceType(lastRequestParams.getPriceType());
      nextRecord.setOpen(open);
      nextRecord.setHigh(high);
      nextRecord.setLow(low);
      nextRecord.setClose(close);
      nextRecord.setEventCount(numEvents);
      nextRecord.setVolume(volume);
      nextRecord.validate();
      newRecords.add(nextRecord);
    }
  }

  /**
   * Returns true if no error was present, false otherwise.
   */
  private boolean processResponseEvent(Event event, Session session) throws Exception {
    MessageIterator msgIter = event.messageIterator();
    while (msgIter.hasNext()) {
      Message msg = msgIter.next();
      if (msg.hasElement(RESPONSE_ERROR)) {
        /*
         * If the security does not exist, skip
         */
        if (msg.getElement(RESPONSE_ERROR).getElementAsString(CATEGORY).equals("BAD_SEC")) {
          errorCtr = 0;
          return true;
        }
        errorCtr++;
        printErrorInfo("REQUEST FAILED: ", msg.getElement(RESPONSE_ERROR));
        /*
         * If the response error is a time out, then reconnect
         */
        if (msg.getElement(RESPONSE_ERROR).getElementAsString(CATEGORY).equals("TIMEOUT")) {
          initializeSession();
        }
        return false;
      }
      processMessage(msg);
    }
    return true;
  }

  public void sendRequest(IntradayBarRequestParams params) throws Exception {
    checkNotNull(session);
    // checkArgument(session.start());
    checkArgument(errorCtr <= MAX_ERROR_COUNT);
    params.validateParams();
    lastRequestParams = params;
    newRecords = new LinkedList<Record>();
    /*
     * Create and send the request...
     */
    Service refDataService = session.getService("//blp/refdata");
    Request request = refDataService.createRequest("IntradayBarRequest");
    request.set("startDateTime",
    /**/params.getStartDate().toString(DATE_TIME_FORMAT_0).replace(" ", "T"));
    request.set("endDateTime", params.getEndDate().toString(DATE_TIME_FORMAT_0).replace(" ", "T"));
    request.set("security", params.getSecurity());
    request.set("eventType", params.getPriceType().toString());
    request.set("interval", params.getInterval());
    request.set("gapFillInitialBar", params.getGapFillInitialBar());
    session.sendRequest(request, null);
    /*
     * Wait for events from session
     */
    eventLoop(session);
  }

  /**
   * Break up the request into smaller time blocks and send. This is done to prevent the session
   * from timing out on large request.
   *
   * @param params
   * @throws Exception
   */
  public void splitAndSendRequest(IntradayBarRequestParams params) throws Exception {
    LocalDate endDate;
    final LocalDate trueStartDate = params.getStartDate();
    final LocalDate trueEndDate = params.getEndDate();
    do {
      endDate = params.getStartDate().plusWeeks(2);
      if (endDate.isAfter(trueEndDate)) {
        endDate = trueEndDate;
      }
      // System.out.println(params.getStartDate() + " - " + endDate);
      params.setEndDate(endDate);
      sendRequest(params);
      params.setStartDate(endDate);
    } while (params.getStartDate().isBefore(trueEndDate));
    params.setStartDate(trueStartDate);
    params.setEndDate(trueEndDate);
  }

  public void stopSession() throws InterruptedException {
    session.stop();
  }
}
