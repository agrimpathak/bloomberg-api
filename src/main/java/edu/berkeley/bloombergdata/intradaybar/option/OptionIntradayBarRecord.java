package edu.berkeley.bloombergdata.intradaybar.option;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.OptionType;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRecord;

public class OptionIntradayBarRecord extends IntradayBarRecord {

  private final static String HEADER_EXPIRATION = "expiration";
  private final static String HEADER_OPTION_TYPE = "call_put_flag";
  private final static String HEADER_STRIKE = "strike";
  private final static String HEADER_UNDERLYING = "underlying";

  public static void initializeHeader() {
    final int FIELD_COUNT = 13;
    header = new String[FIELD_COUNT];
    int i = 0;
    header[i++] = HEADER_TIMESTAMP;
    header[i++] = HEADER_SECURITY;
    header[i++] = HEADER_PRICE_TYPE;
    header[i++] = HEADER_OPEN;
    header[i++] = HEADER_HIGH;
    header[i++] = HEADER_LOW;
    header[i++] = HEADER_CLOSE;
    header[i++] = HEADER_VOLUME;
    header[i++] = HEADER_EVENTCOUNT;
    header[i++] = HEADER_UNDERLYING;
    header[i++] = HEADER_EXPIRATION;
    header[i++] = HEADER_STRIKE;
    header[i++] = HEADER_OPTION_TYPE;
  }

  void setExpiration(LocalDate expiration) {
    setGeneric(HEADER_EXPIRATION, expiration.toString());
  }

  void setOptionType(OptionType optionType) {
    setGeneric(HEADER_OPTION_TYPE, "" + optionType.getName());
  }

  void setStrike(double strike) {
    setGeneric(HEADER_STRIKE, "" + strike);
  }

  void setUnderlying(String underlying) {
    setGeneric(HEADER_UNDERLYING, underlying);
  }
}
