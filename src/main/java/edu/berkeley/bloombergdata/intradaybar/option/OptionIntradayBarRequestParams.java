package edu.berkeley.bloombergdata.intradaybar.option;

import static edu.berkeley.bloombergdata.util.Formatting.MM_DD_YYYY;
import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;
import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import org.joda.time.LocalDate;

import edu.berkeley.bloombergdata.commons.OptionType;
import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequestParams;

public class OptionIntradayBarRequestParams extends IntradayBarRequestParams {

  private LocalDate expirationDate;
  private OptionType optionType;
  private double strike;
  private String suffix;
  private String underlying;
  private String underlyingExclEquity;

  public void checkArgs() {
    checkNotNull(expirationDate, optionType);
    checkArgument(strike > 0);
    checkArgument(!underlying.isEmpty());
  }

  public LocalDate getExpirationDate() {
    return expirationDate;
  }

  public OptionType getOptionType() {
    return optionType;
  }

  @Override
  public String getSecurity() {
    checkArgs();
    StringBuilder securityBuilder = new StringBuilder();
    securityBuilder.append(underlyingExclEquity);
    securityBuilder.append(" ");
    securityBuilder.append(expirationDate.toString(MM_DD_YYYY));
    securityBuilder.append(" ");
    securityBuilder.append(optionType.getName());
    securityBuilder.append(strike);
    securityBuilder.append(" ");
    securityBuilder.append(suffix);
    return securityBuilder.toString();
  }

  public double getStrike() {
    return strike;
  }

  public String getUnderlying() {
    return underlying;
  }

  public void setExpirationDate(LocalDate expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setOptionType(OptionType optionType) {
    this.optionType = optionType;
  }

  public void setStrike(double strike) {
    this.strike = strike;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  public void setUnderlying(String underlying) {
    this.underlying = underlying;
    /*
     * Remove "Equity" from underlying if present
     */
    String[] underlyingParts = underlying.split("\\s+");
    StringBuilder underlyingBuilder = new StringBuilder();
    for (String underlyingPart : underlyingParts) {
      if (underlyingPart.toLowerCase().equals("equity")) {
        continue;
      }
      underlyingBuilder.append(underlyingPart);
      underlyingBuilder.append(" ");
    }
    underlyingExclEquity = underlyingBuilder
    /**/.delete(-1 + underlyingBuilder.length(), underlyingBuilder.length())
    /**/.toString();
  }

  public double strike() {
    return strike;
  }

  /* Do not call super.validateParams() */
  @Override
  public void validateParams() {
    checkNotNull(startDate, endDate);
    checkNotNull(priceType);
    checkArgs();
  }
}
