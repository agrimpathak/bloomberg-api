package edu.berkeley.bloombergdata.intradaybar.option;

import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;

import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Message;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequester;

public class OptionIntradayBarRequester extends IntradayBarRequester {

  @Override
  protected void processMessage(Message msg) throws Exception {
    Element data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA);
    int numBars = data.numValues();
    OptionIntradayBarRecord nextRecord;
    checkArgument(lastRequestParams instanceof OptionIntradayBarRequestParams);
    OptionIntradayBarRequestParams _lastRequestParams =
    /**/(OptionIntradayBarRequestParams) lastRequestParams;
    for (int i = 0; i < numBars; ++i) {
      Element bar = data.getValueAsElement(i);
      Datetime time = bar.getElementAsDate(TIME);
      double open = bar.getElementAsFloat64(OPEN);
      double high = bar.getElementAsFloat64(HIGH);
      double low = bar.getElementAsFloat64(LOW);
      double close = bar.getElementAsFloat64(CLOSE);
      int numEvents = bar.getElementAsInt32(NUM_EVENTS);
      long volume = bar.getElementAsInt64(VOLUME);

      nextRecord = new OptionIntradayBarRecord();
      nextRecord.setDatetime(time);
      nextRecord.setSecurity(_lastRequestParams.getSecurity());
      nextRecord.setPriceType(_lastRequestParams.getPriceType());
      nextRecord.setOpen(open);
      nextRecord.setHigh(high);
      nextRecord.setLow(low);
      nextRecord.setClose(close);
      nextRecord.setEventCount(numEvents);
      nextRecord.setVolume(volume);
      nextRecord.setUnderlying(_lastRequestParams.getUnderlying());
      nextRecord.setExpiration(_lastRequestParams.getExpirationDate());
      nextRecord.setStrike(_lastRequestParams.getStrike());
      nextRecord.setOptionType(_lastRequestParams.getOptionType());
      nextRecord.validate();
      newRecords.add(nextRecord);
    }
  }
}
