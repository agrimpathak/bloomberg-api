package edu.berkeley.bloombergdata.intradaybar.bond;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRecord;

public class TreasuryIntradayBarRecord extends IntradayBarRecord {

  private final static String HEADER_COUPON = "coupon";
  private final static String HEADER_MATURITY = "maturity";

  public static void initializeHeader() {
    final int FIELD_COUNT = 11;
    header = new String[FIELD_COUNT];
    int i = 0;
    header[i++] = HEADER_TIMESTAMP;
    header[i++] = HEADER_SECURITY;
    header[i++] = HEADER_PRICE_TYPE;
    header[i++] = HEADER_OPEN;
    header[i++] = HEADER_HIGH;
    header[i++] = HEADER_LOW;
    header[i++] = HEADER_CLOSE;
    header[i++] = HEADER_VOLUME;
    header[i++] = HEADER_EVENTCOUNT;
    header[i++] = HEADER_MATURITY;
    header[i++] = HEADER_COUPON;
  }

  void setCoupon(String coupon) {
    setGeneric(HEADER_COUPON, coupon);
  }

  void setMaturity(String maturity) {
    setGeneric(HEADER_MATURITY, maturity);
  }
}
