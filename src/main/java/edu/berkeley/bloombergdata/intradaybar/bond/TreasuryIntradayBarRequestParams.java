package edu.berkeley.bloombergdata.intradaybar.bond;

import static edu.berkeley.bloombergdata.util.Preconditions.checkNotNull;

import edu.berkeley.bloombergdata.intradaybar.IntradayBarRequestParams;

public class TreasuryIntradayBarRequestParams extends IntradayBarRequestParams {

  private String coupon;
  private String maturity;

  public String getCoupon() {
    return coupon;
  }

  public String getMaturity() {
    return maturity;
  }

  public void setCoupon(String coupon) {
    this.coupon = coupon;
  }

  public void setMaturity(String maturity) {
    this.maturity = maturity;
  }

  @Override
  public void validateParams() {
    super.validateParams();
    checkNotNull(coupon, maturity);
  }
}
