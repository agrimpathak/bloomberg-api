package edu.berkeley.bloombergdata.export;

import java.util.Map;

public interface Record {

  /**
   * @return Returns the records entries in order.
   */
  Map<String, Object> getEntries();

  /**
   * @return Returns the field names
   */
  String[] header();

  /**
   * Ensure data integrity as much as possible.
   */
  void validate();
}
