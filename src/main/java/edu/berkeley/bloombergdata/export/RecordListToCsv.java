package edu.berkeley.bloombergdata.export;

import static edu.berkeley.bloombergdata.util.Preconditions.checkArgument;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

public class RecordListToCsv {

  public static void writeCsv(List<Record> records, String path) throws IOException {
    checkArgument((records != null) && (records.size() > 0));
    checkArgument((path != null) && !path.isEmpty());
    /*
     * Extract the header from the first record
     */
    Iterator<Record> iter = records.iterator();
    Record next = iter.next();
    final String[] header = next.header();
    /*
     * Initialize the MapWriter
     */
    ICsvMapWriter mapWriter = null;
    try {
      mapWriter = new CsvMapWriter(new FileWriter(path), CsvPreference.STANDARD_PREFERENCE);
      /*
       * Write the header, and the first record
       */
      mapWriter.writeHeader(header);
      mapWriter.write(next.getEntries(), header);
      next = null;
      /*
       * Write the rest of the records
       */
      while (iter.hasNext()) {
        mapWriter.write(iter.next().getEntries(), header);
      }
    } finally {
      if (mapWriter != null) {
        mapWriter.close();
      }
    }
  }
}
