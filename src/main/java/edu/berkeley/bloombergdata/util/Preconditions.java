package edu.berkeley.bloombergdata.util;

public class Preconditions {

  public static void checkArgument(boolean b) {
    if (!b) {
      throw new IllegalArgumentException();
    }
  }

  public static void checkArgument(boolean b, String msg) {
    if (!b) {
      throw new IllegalArgumentException(msg);
    }
  }

  public static void checkNotNull(Object obj) {
    if (obj == null) {
      throw new IllegalArgumentException();
    }
  }

  public static void checkNotNull(Object... objects) {
    for (Object obj : objects) {
      checkNotNull(obj);
    }
  }

  public static void checkNotNull(Object obj, String msg) {
    if (obj == null) {
      throw new IllegalArgumentException(msg);
    }
  }

  public static void checkNotNull(String msg, Object... objects) {
    for (Object obj : objects) {
      checkNotNull(obj, msg);
    }
  }
}
