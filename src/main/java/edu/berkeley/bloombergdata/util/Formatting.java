package edu.berkeley.bloombergdata.util;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Formatting {

  public static final DateTimeFormatter DATE_TIME_FORMAT_0 = DateTimeFormat
  /**/.forPattern("yyyy-MM-dd 00:00:00");
  public static final DateTimeFormatter MM_DD_YYYY = DateTimeFormat
  /**/.forPattern("MM/dd/yyyy");
  public static final DateTimeFormatter YYYYMMDD = DateTimeFormat
  /**/.forPattern("yyyyMMdd");
}
